import Foundation

// MARK: 1. Basic Closure
func closure(action: () -> Void) {
    print("I'm in")
    action()
    print("I'm out")
}

print("\n1. Basic Closure")

closure(action: {
    print("Hello")
})
closure {
    print("Hello")
}
closure() {
    print("Hello")
}


// MARK: 2. Parametered Closure
// Addition: Always remember weak self in class
func getSumOf(array: [Int], handler: ((Int) -> Void)) {
    print("Step 2")
    var sum: Int = 0
    
    for value in array {
        sum += value
    }
    
    print("Step 3")
    handler(sum)
}

print("\n2. Parametered Closure")
print("Step 1")
getSumOf(array: [1, 2, 3, 4, 5]) { sum in
    print(sum)
    print("Step 4")
}

// MARK: 3. Escaping Closure (Storage)
// Explanation: It use escaping because the sum3 use reference to sum object
//              defined in the closure. Since it's in there, it'll vanish
//              using escaping to define it'll remain until super scope end
// Reference: Escaping Closure (Async) <- Other example
var complitionHandler: ((Int)->Void)?
var sum3: Int = 0

func getSumOf3(array:[Int], handler: @escaping ((Int)->Void)) {
    var sum: Int = 0
    
    for value in array {
        sum += value
    }
    
    sum3 = sum
    complitionHandler = handler
}

func doSomething() {
    getSumOf3(array: [1, 2, 3, 4, 5]) { sum in
        print(sum)
    }
}

print("\n3. Escaping Closure (Storage)")
doSomething()
complitionHandler!(sum3)

// MARK: 4. Testing complex
public func multiParam(from sender: () -> Int, to receiver: () -> String) -> String {

    if sender() == 1 {
        return "Success sending to John Doe"
    }
    
    return "Success sending to \(receiver)"
}

print("\n4. Testing complex")
print(multiParam(from: {
    return 1
}, to: {
    return "Andrea"
}))

// MARK: 5. Multiple closure paradigm
// Explanation: Only the last closure parameter can defined outside of func param when
//              initialized. Example here is receiver can be outside caller
public func multiParam2(from sender: () -> Int, to receiver: () -> String) -> String {
    
    if sender() == 1 {
        return "Success sending to John Doe"
    }
    
    return "Success sending to \(receiver())"
}

print("\n5. Multiple closure paradigm")
print(multiParam2(from: {
    return 0
}) {
    return "Andrea"
})
