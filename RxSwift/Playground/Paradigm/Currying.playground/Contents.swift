
// MARK: 1. Normal function
func add (a: Int, b: Int) -> Int {
    return a + b;
}

print("1. Normal function")
print(add(a: 3, b: 4))

// MARK: 2. Currying
func multiply2(_ x: Int) -> (_ y: Int) -> Int {
    // Cannot use y, because it's in curry
    return { $0 * x }
}

print("\n2. Currying")
print(multiply2(3)(4))
