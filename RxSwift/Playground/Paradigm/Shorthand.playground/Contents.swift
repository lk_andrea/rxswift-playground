
// 1. Normal
func multiAddition(handler: ((Int, Int, Int) -> Void)) {
    handler(1, 2, 3)
}

print("1. Normal")
multiAddition { x, y, z in
    print(x)
    print(y)
    print(z)
}

// 2. Closure Shorthand
// Explaination : Example in Closure, it doesn't need to be defined. But need all shorthand
//                to be used in operation
func getSumOf(handler: ((Int, Int, Int) -> Void)) {
    handler(1, 2, 3)
}

print("\n2. Shorthand")
getSumOf {
    print($0)
    print($1)
    print($2)
}
