import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

// MARK: 1. Basic Observable
example(of: "1. Basic Obserable") {
    let one = 1
    let two = 2
    let three = 3
    
    // Just one element
    let _: Observable<Int> = Observable<Int>.just(one)
    
    // Casting as int because it takes one by one element, it considered as 3 element
    let _ = Observable.of(one, two, three)
    // Example as 1 element array typed
    let _ = Observable.of([one, two, three])
    
    // Take 1 - 1 element inside 1 array using FROM
    let _ = Observable.from([one, two, three])
}
