import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

// MARK: 1. Basic Subscribe
example(of: "1. Basic Subscribe") {
    let observable = Observable.of(1, 2, 3)
    
    // Subscribe for all event, manual handling for event type
    observable.subscribe() { event in
        print(event)
        
        // Handling nil element such as .completed event with nil value
        if let element = event.element {
            print(element)
        }
    }
}

// MARK: 2. Subscribe specific
example(of: "2. Subscribe specific") {
    let observable = Observable.of(1, 2, 3)
    
    // Subscribe for specific event using closure
    observable.subscribe(onNext: { element in
        print(element)
    })
}
