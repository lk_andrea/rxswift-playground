import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

// MARK: 1. Basic Dispose
// Explanation: Just to make sure it's freeing up memory, avoiding memory leak
example(of: "1. Basic Dispose") {
    let observable = Observable<String>.of("A", "B", "C")
    // Inferred type
    // let observable2 = Observable.of("A", "B", "C")

    let subscription = observable.subscribe { event in
        print(event)
    }

    subscription.dispose()
}

// MARK: 2. Dispose Bag
// Explanation: Store in DisposeBag, all subscribtion cleaned when dispose is cleaned. The subsription dies with disposeBag scope
example(of: "2. DisposeBag") {
    let disposeBag = DisposeBag()
    
    Observable.of("A", "B", "C").subscribe {
        if let element = $0.element {
            print($0, ", value:", element)
        }
        else {
            print($0)
        }
    }.addDisposableTo(disposeBag)
}

// MARK: 3. Testing sequnce
// Explanation: Observable will finish when meet termination either onError / onCompleted / disposed. If there's element after that termination, it'll never executed. If there's no termination, than it's leaked of memory sooner or later
example(of: "3. Testing sequence") {
    let disposeBag = DisposeBag()
    
    enum MyError: Error {
        case anError
    }
    
    Observable<String>.create { observer in
        observer.onNext("1")
        observer.onError(MyError.anError) // End
        observer.onCompleted() // End
        observer.onNext("?")
        
        return Disposables.create()
    }.subscribe(onNext: {
        print("Next", $0)
    }, onError: {
        print("Error", $0)
    }, onCompleted: {
        print("Completed")
    }, onDisposed: {
        print("Disposed")
    }).addDisposableTo(disposeBag) // End
}
