import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

// MARK: 1. Basic factories and deferred
example(of: "1. Basic factories and deferred") {
    let disposeBag = DisposeBag()
    
    var flip = false
    let factory: Observable<Int> = Observable.deferred {
        flip = !flip
        
        if flip {
            return Observable.of(1, 2, 3)
        } else {
            return Observable.of(4, 5, 6)
        }
    }
    
    for index in 0...2 {
        print()
        factory.subscribe(onNext: {
            print("Index:", index, ", value:", $0)
        }).addDisposableTo(disposeBag)
    }
}
