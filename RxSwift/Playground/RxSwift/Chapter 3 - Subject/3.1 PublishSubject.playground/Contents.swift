import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. Publish Subject
// Explanation: Observable and Observer becoming one, it's appending to variable and when it does it notify who subscribe it. Lifetime until subject / subscriber been terminated (complete / error / dispose)
example(of: "1. Publish Subject") {
    let subject = PublishSubject<String>()
    
    // Not going to print, because no one listening / subscribing yet
    subject.onNext("Is anyone listening?")
    let _ = subject.subscribe(onNext: { string in
        print("First subscriber: ", string)
    }, onCompleted: {
        print("First subscriber out")
    })
    
    // Normally Observable cannot append because it's already defined. With subject we can achieve that.
    // It's the same
    subject.on(.next("1"))
    subject.onNext("2")
    
    // Getting two listener here
    let secondSubscriber = subject.subscribe(onNext: { string in
        print("Secod subscriber: ", string)
    }, onCompleted: {
        print("Secod subscriber out")
    }, onDisposed: {
        print("Secod subscriber disposed")
    })
    
    subject.on(.next("3"))
    subject.on(.next("4"))
    
    // Let's end secondSubscriber listener, to terminate use dispose single on secondSubscriber
    secondSubscriber.dispose()
    subject.on(.next("5"))
    
    subject.onError(MyError.errorHi) // This give error to Subject, and througout to all subscriber
    subject.on(.completed) // Ending for subject sequence
    
    // subject.dispose() // Ending to third subsriber if it's here, creates error different from complete
    
    // Getting three listener here, but it's completed the sequence already
    let _ = subject.subscribe(onNext: { string in
        print("Third subscriber: ", string)
    }, onCompleted: {
        print("Third subscriber out")
    })
    
    // It's better take a precaution for error / completed subject like validation if it applicable. Since subject didn't give choice for termination checking because dangerous
    // Haven't tried, no idea how to validate
    
    //if subject.takeLast(1) == .completed {
    //    print("-- Subject has been disposed")
    //}
    
    // The process seems get the last element first to check, if it's stuck in error then the listener doesn't proceed next
    subject.on(.next("4"))
}
