import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// Mark: 1. Behavior Subject
// Explanation: Basicaly same as Publish except it replays the last element
example(of: "1. Behavior Subject") {
    let subject = BehaviorSubject(value: "Initial value")
    let disposeBag = DisposeBag()
    
    subject.subscribe { event in
        if let _ = event.element {
            print(event.element!)
        } else if let _ = event.error {
            print(event.error!)
        } else {
            print(event)
        }
    }.addDisposableTo(disposeBag)
    
    subject.on(.next("X"))
    subject.on(.next("A"))
    subject.on(.error(MyError.errorHi)) // Comment this out to find more
    subject.on(.next("B"))
    
    // Normally it didn't print anything, but in behaviour the last element being processed
    subject.subscribe { event in
        print("Secod subs: ", event)
    }
    
    // Same as above
    subject.subscribe(onNext: { string in
        print(string)
    }, onError: { string in
        print(string)
    })
}
