import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// Mark: 1. Replay Subject
// Explanation: Replay all elements when it is subsribed by others. We define the buffered replay size
example(of: "1. Replay Subject") {
    let subject = ReplaySubject<String>.create(bufferSize: 2)
    let disposeBag = DisposeBag()
    
    // First subsriber
    subject.subscribe(onNext: { string in
        print("First", string)
    }).addDisposableTo(disposeBag)
    
    subject.on(.next("1"))
    subject.on(.next("2"))
    subject.on(.next("3"))
    subject.on(.next("4"))
    print()
    
    // Second subsriber
    // Replay 3, 4
    subject.subscribe(onNext: { string in
        print("Secod", string)
    }).addDisposableTo(disposeBag)
    
    subject.on(.error(MyError.errorHi)) // Make it stuck!
    subject.on(.next("5"))
    subject.dispose()
    print()
    
    // Third subsriber
    // This will not emits anything since it has been disposed
    subject.subscribe(onNext: { string in
        print("Third", string)
    }).addDisposableTo(disposeBag)
    
    subject.on(.next("6"))
    subject.on(.next("7"))
}
