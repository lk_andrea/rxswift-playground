import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// Mark: 1. Variables
// Explanation: Same as behaviourSubject just different in method aside from easier naming. Example of usage in array or collection. Mainly used as a base object / variable, so it'll make the super object become Observable easier.
example(of: "1. Variables") {
    // We use let because the base is Variable and didn't change. Just the property changed
    let variable = Variable("Initial Value")
    let disposeBag = DisposeBag()
    
    // Creating next element
    variable.value = "New initial value"
    variable.asObservable()
        .subscribe {
            print("1", $0.element ?? "")
        }.addDisposableTo(disposeBag)
    
    // Getting second subsriber
    variable.value = "Hahaha"
    variable.asObservable()
        .subscribe {
            print("2", $0.element ?? "")
        }.addDisposableTo(disposeBag)
    
    variable.value = "Test"
}
