import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. ToArray
example(of: "1. ToArray") {
    let disposeBag = DisposeBag()
    
    // Normally
    Observable.of("A", "B", "C")
        .subscribe(onNext: {
            print($0)
        })
        .addDisposableTo(disposeBag)
    
    // As an array
    Observable.of("A", "B", "C")
        .toArray()
        .subscribe(onNext: {
            print($0)
        })
        .addDisposableTo(disposeBag)
}
