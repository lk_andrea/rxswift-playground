import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. Map
example(of: "1. Map") {
    let disposeBag = DisposeBag()
    let sequence = Observable.of(1, 3, 5)
    
    // Normal Way
    sequence.subscribe(onNext: { element in
        print(element * 10)
    }).addDisposableTo(disposeBag)
    
    // Map Way
    print()
    sequence.map { $0 * 10 }
        .subscribe(onNext: { element in
            print(element)
        })
        .addDisposableTo(disposeBag)
}
