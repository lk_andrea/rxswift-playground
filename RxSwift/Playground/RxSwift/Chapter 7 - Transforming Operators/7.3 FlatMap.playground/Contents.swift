import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

struct Student {
    var score: Variable<Int>
    var grade: Int
}

// MARK: 1. FlatMap
example(of: "1. FlatMap") {
    let disposeBag = DisposeBag()
    
    let ryan = Student(score: Variable(80), grade: 2)
    var charlotte = Student(score: Variable(90), grade: 3)
    
    // Subject can be done 2 ways as we know it, as observer and as observable. It's defined in the base, meanwhile the method asObservable indicates it as an Object. The asObserver method will act as an Object Class to access the lower element.
    let students = PublishSubject<Student>()
    
    // We use the flatMap for accesing the Variable type. It's defined as "Project observable sequence"
    students.asObservable().flatMap {
            $0.score.asObservable()
        }.subscribe(onNext: {
            print("First", $0)
        }).addDisposableTo(disposeBag)
    
    // If we want to access the normal variable type, we must cast it as an Observable Sequence
    students.asObservable().flatMap {
            Observable.of($0.grade)
        }.subscribe(onNext: {
            print("Second", $0)
        }).addDisposableTo(disposeBag)
    
    // While we can do normally with map
    students.asObservable().map {
            $0.grade
        }.subscribe(onNext: {
            print("Third", $0)
        }).addDisposableTo(disposeBag)
    
    // But if we had to access Variable Subject with map, we can do with value
    students.asObservable().map {
            $0.score.value
        }.subscribe(onNext: {
            print("Fourth", $0)
        }).addDisposableTo(disposeBag)
    
    students.onNext(ryan)
    print()
    students.onNext(charlotte)
    print()
    
    // Let's change the student score
    // Explanation: The listener would be only the First since it remakes the score as an Observable. The other method didn't designed to listen for further changes
    charlotte.score.value = 100
    
    // And the grade:
    // Explanation: The grade didn't get listener because it's not defined as an Observable. That's why we need the Variable Subject
    charlotte.grade = 5
}
