import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

struct Student {
    var score: Variable<Int>
}

// MARK: 1. FlatMapLatest
example(of: "1. FlatMapLatest") {
    let disposeBag = DisposeBag()
    
    let ryan = Student(score: Variable(11))
    let charlotte = Student(score: Variable(22))
    
    let student = PublishSubject<Student>()
    
    student.asObservable().flatMapLatest {
            $0.score.asObservable()
        }.subscribe(onNext: {
            print($0)
        }).addDisposableTo(disposeBag)
    
    student.onNext(ryan)
    ryan.score.value = 13
    
    // Subscribe the latest element in student. Therefore Ryan will be ignored even something is happening
    student.onNext(charlotte)
    
    ryan.score.value = 14
    charlotte.score.value = 25
    ryan.score.value = 16
    charlotte.score.value = 27
    charlotte.score.value = 28
}
