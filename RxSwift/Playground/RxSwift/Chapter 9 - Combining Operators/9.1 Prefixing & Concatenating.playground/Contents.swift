import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. StartWith
example(of: "1. StartWith") {
    let numbers = Observable.of(2, 3, 4)
    let observable = numbers.startWith(1)
    
    observable.subscribe(onNext: { value in
        print(value)
    })
}
