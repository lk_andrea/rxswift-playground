import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. Merge
example(of: "1. Merge") {
    // There's 2 different subject
    let left = PublishSubject<String>()
    let right = PublishSubject<String>()
    
    // We make a new observable that consist of 2 element, just the constant element. The element become an array of Subjects
    let source = Observable.of(left.asObservable(), right.asObservable())
    source.subscribe(onNext: { value in
        print(value)
    }).dispose()
    
    // Then we make it as a new Sequence of Observable
    let observable = source.merge()
    let disposable = observable.subscribe(onNext: { value in
        print(value)
    })
    
    print()
    left.onNext("A")
    right.onNext("B")
    left.onNext("C")

    // Because it's a variable, we can dispose it by this or in DisposeBag variable
    disposable.dispose()
}
