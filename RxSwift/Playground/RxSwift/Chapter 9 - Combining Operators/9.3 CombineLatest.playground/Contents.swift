import RxSwift

func example(of description: String, action: () -> Void) {
    print("\n\(description)")
    action()
}

enum MyError: Error {
    case errorHi
}

// MARK: 1. Combine Latest
// Explanation: It combine the element in an Selector (Observable Builder) with the latest element on each Sequence
example(of: "1. Combine Latset") {
    let left = PublishSubject<String>()
    let right = PublishSubject<String>()
    
    // This can happen because the combineLatest closure will be wrapped in Observable. So it expect to be an element, not a command execution. The return can be various types like Int or String
    let observable = Observable.combineLatest(left, right) { lastLeft, lastRight in
        // Example 1, Accepted:
        // "asd" + "qwe"
        // Example 2, Not accepted but executed
        // print("asd")
        // Example 3, Accepted:
        // "\(lastLeft) \(lastRight)"
        
        lastLeft + " " + lastRight
    }
    
    // Then we subsribe the Observable Builder like
    let disposable = observable.subscribe(onNext: { value in
        print(value)
    })
    
    // This will not be combined because the other value still nil
    print("1")
    left.onNext("Hello")
    
    // Combination started
    print("2")
    right.onNext("world")
    print("3")
    right.onNext("RxSwift")
    print("4")
    left.onNext("Have a good day,")
    
    disposable.dispose()
}

// MARK: 2. Custom
example(of: "2. Custom") {
    let left = PublishSubject<String>().startWith("left")
    let right = PublishSubject<String>().startWith("right")
    
    // We can make two elements in Observable like array type
    Observable.combineLatest(left, right) {
            ($0, $1)
        }.subscribe(onNext: { left, right in
            print(left, right)
        }).dispose()
    
    // If we make a filter
    Observable.combineLatest(left, right) {
        ($0, $1)
        }.filter { !$0.0.isEmpty }.subscribe(onNext: { left, right in
            print(left, right)
        }).dispose()
    
    // Parameter naming
    Observable.combineLatest(left, right) {
        (greeting: $0, noun: $1)
        }.filter{ !$0.greeting.isEmpty }.subscribe(onNext: { left, right in
            print(left, right)
        }).dispose()
    
}
