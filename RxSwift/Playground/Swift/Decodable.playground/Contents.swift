import Foundation

// Creating the Decodable Object
struct Car: Decodable {
    let name: String
    let horsepower: Int
}

// MARK: 1. Basic Decoder
do {
    // Do will just scoping our code
    // Trying to get Remote JSON URL
    let url = URL(string: "https://demo0989623.mockable.io/car/1")!
    
    // Save as an task
    let task = URLSession.shared.dataTask(with: url) { data, response, error in
        // Basicly guard same as bunch of If
        guard error == nil else {
            print("error: \(error!)")
            return
        }
        
        guard let data = data else {
            print("No Data")
            return
        }
        
        guard let car = try? JSONDecoder().decode(Car.self, from: data) else {
            print("Error: Couldn't decode data into car")
            return
        }
        
        print("Gotten car is \(car)")
    }
    
    // Task resume, on this point it's going to start executing
    task.resume()
}

// MARK: 2. Multiple Decoder
// Decode into multiple struct
do {
    let _ = URLSession.shared.dataTask(with: URL(string: "https://demo0989623.mockable.io/cars")!) { data, response, error in
        
        guard let car = try? JSONDecoder().decode([Car].self, from: data!) else {
            return
        }
        
        print()
        for item in car {
            print(item.name)
        }
    }.resume()
}
