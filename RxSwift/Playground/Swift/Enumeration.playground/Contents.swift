import UIKit

// MARK: Base Enum
enum CompassPoint
{
    case north
    case south
    case east
    case west
}

var direction = CompassPoint.north
switch direction
{
case .north:
    print("utara")
case .south:
    print("selatan")
case .east:
    print("timur")
case .west:
    print("barat")
}

// MARK: Enum with return
enum Kompass: String
{
    case utara = "Seribu"
    case barat = "Tangerang"
    case selatan = "Bogor"
    case timur = "Bekasi"
}

var tempat = Kompass.barat
print(tempat)

// MARK: Recursive
enum Arithmetic
{
    case number(Int)
    indirect case addition(Arithmetic, Arithmetic)
    indirect case multiplication(Arithmetic, Arithmetic)
}

let five = Arithmetic.number(5)
let sum = Arithmetic.addition(Arithmetic.number(5), five)

func evaluate(_ expression: Arithmetic) -> Int {
    switch expression {
    case let .number(value):
        return value
    case let .addition(left, right):
        return evaluate(left) + evaluate(right)
    case let .multiplication(left, right):
        return evaluate(left) * evaluate(right)
    }
}

print(evaluate(sum))
