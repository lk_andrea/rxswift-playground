import UIKit
import RxSwift

// MARK: Define struct
struct Person {
    let name: String
    var desc: String?
    
    public init(name: String) {
        self.name = name
    }
    
    public func greeting() {
        print("Hello")
    }
}

// MARK: Base struct
var Andrea = Person(name: "Andrea")
print(Andrea.name)
Andrea.desc = "Ho"

print(Andrea.desc!)
Andrea.greeting()

// MARK: Array of struct
var listPeople: [Person] = []

listPeople.append(Person(name: "Andrea"))
listPeople.append(Person(name: "JO"))

listPeople[1].name
