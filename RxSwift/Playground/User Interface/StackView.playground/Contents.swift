import UIKit
import PlaygroundSupport
import SnapKit

class MyViewController : UIViewController {
    weak var titleLabel: UILabel!
    weak var buttonView: UIButton!
    weak var stackView: UIStackView!
    weak var labelEnd: UILabel!
    weak var tableView: UITableView!
 
    @objc func ButtonXTapped(_ sender: UIButton) {
        let addLabel: UILabel = {
            let addLabel = UILabel()
            addLabel.text = "Haha"
            addLabel.textColor = .black
            return addLabel
        }()
        
        stackView.addArrangedSubview(addLabel)
        
        let addLabel2: UILabel = {
            let addLabel = UILabel()
            addLabel.text = "0"
            addLabel.textColor = .black
            return addLabel
        }()
        
        stackView.insertArrangedSubview(addLabel2, at: 0)
    }
    
    override func loadView() {
        let view: UIView = {
            let view = UIView()
            view.backgroundColor = .white
            view.isUserInteractionEnabled = true
            return view
        }()
        self.view = view
        
        let tableView: UITableView = {
            let tableView = UITableView()
            return tableView
        }()
        self.tableView = tableView
        self.view.addSubview(tableView)
        
        let label: UILabel = {
            let label = UILabel()
            label.backgroundColor = .red
            label.text = "Hello World!"
            label.textColor = .black
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        self.titleLabel = label
        self.tableView.addSubview(label)
      
        let button: UIButton = {
            let button = UIButton()
            button.setTitle("Click", for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.backgroundColor = #colorLiteral(red: 0.5450980392, green: 0.5450980392, blue: 0.5450980392, alpha: 1)
            button.addTarget(self, action: #selector(ButtonXTapped(_:)), for: .touchUpInside)
            return button
        }()
        self.buttonView = button
        self.tableView.addSubview(button)
        
        let stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.axis = .vertical
            stackView.distribution = .fillEqually
            return stackView
        }()
        self.stackView = stackView
        self.tableView.addSubview(stackView)
        
        let labelEnd: UILabel = {
            let label = UILabel()
            label.backgroundColor = .red
            label.text = "End"
            label.textColor = .black
            label.textAlignment = .center
            return label
        }()
        self.labelEnd = labelEnd
        self.tableView.addSubview(labelEnd)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.snp.makeConstraints{ make in
            make.center.equalToSuperview().offset(30)
        }
        
        self.titleLabel.snp.makeConstraints{ make in
            make.height.equalTo(50)
            make.width.equalTo(50)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
        }
        
        self.buttonView.snp.makeConstraints{ make in
            make.top.equalTo(titleLabel!.snp_bottomMargin).offset(50)
            make.centerX.equalToSuperview()
        }
        
        self.stackView.snp.makeConstraints{ make in
            make.top.equalTo(buttonView.snp_bottomMargin)
            make.centerX.equalToSuperview()
        }
        
        self.labelEnd.snp.makeConstraints{ make in
            make.top.equalTo(stackView.snp_bottomMargin)
            make.width.equalTo(100)
            make.height.equalTo(100)
            make.centerX.equalToSuperview()
        }
    }
}

// Present the view controller in the Live View window
PlaygroundPage.current.needsIndefiniteExecution = true
PlaygroundPage.current.liveView = MyViewController()
