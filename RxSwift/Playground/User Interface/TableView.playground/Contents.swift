import UIKit
import PlaygroundSupport

class CustomCell: UITableViewCell {
    weak var label1: UILabel!
    weak var button1: UIButton!
    
    @objc func ButtonXTapped(_ sender: UIButton) {
        print("Button Clicked")
        label1.isHidden = true
        
        let addLabel: UILabel = {
            let addLabel = UILabel()
            addLabel.text = "Haha"
            addLabel.textColor = .black
            return addLabel
        }()
        
        contentView.addSubview(addLabel)
        
        NSLayoutConstraint.activate([
            addLabel.topAnchor.constraint(equalTo: label1.topAnchor, constant: 10),
            addLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            addLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 10),
            addLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 10)
        ])
        
        self.contentView.layoutIfNeeded()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.isEditing = true
        
        let label1: UILabel = {
            let label1 = UILabel()
            label1.translatesAutoresizingMaskIntoConstraints = false
            label1.textAlignment = .center
            label1.backgroundColor = #colorLiteral(red: 0.572549045085907, green: 0.0, blue: 0.23137255012989, alpha: 1.0)
            label1.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
            contentView.addSubview(label1)
            self.label1 = label1
        
            return label1
        }()
        
        let button1: UIButton = {
            let button1 = UIButton()
            button1.translatesAutoresizingMaskIntoConstraints = false
            button1.backgroundColor = #colorLiteral(red: 0.0901960805058479, green: 0.0, blue: 0.301960796117783, alpha: 1.0)
            button1.addTarget(self, action: #selector(self.ButtonXTapped(_:)), for: .touchUpInside)
            
            self.button1 = button1
            contentView.addSubview(button1)
            
            return button1
        }()
        
        NSLayoutConstraint.activate([
            label1.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            button1.leadingAnchor.constraint(equalTo: label1.trailingAnchor, constant: 10),
            contentView.trailingAnchor.constraint(equalTo: button1.trailingAnchor, constant: 10),
            label1.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            button1.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            contentView.bottomAnchor.constraint(equalTo: label1.bottomAnchor, constant: 10),
            contentView.bottomAnchor.constraint(equalTo: button1.bottomAnchor, constant: 10),
            label1.widthAnchor.constraint(equalTo: button1.widthAnchor)
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ViewController: UITableViewController {
    var objects: [String] = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .spellOut
        return Array(0 ..< 3).compactMap { formatter.string(from: NSNumber(value: $0)) }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(CustomCell.self, forCellReuseIdentifier: "CustomCell")
        
        // set it up to let auto-layout resize the cell
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.label1.text = objects[indexPath.row]
        cell.button1.setTitle("Click", for: .normal)
        
        return cell
    }
}

PlaygroundPage.current.liveView = ViewController()
